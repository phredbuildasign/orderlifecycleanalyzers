﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LINQtoCSV;

namespace GetShipmentIds
{
    class Program
    {
        private class DbCorrelationInfo
        {
            public Guid CorrelationId { get; set; }
            public string OrderId { get; set; }
            public DateTime OrderCreatedDate { get; set; }
        }


        private class ErrorCorrelationInfo
        {
            [CsvColumn(Name = "Date", FieldIndex = 1)]
            public string Date { get; set; }
            [CsvColumn(Name = "Message", FieldIndex = 3)]
            public string Message { get; set; }
            [CsvColumn(Name = "Error Message", FieldIndex = 4)]
            public string ErrorMessage { get; set; }
            [CsvColumn(Name = "Stack Trace", FieldIndex = 5)]
            public string StackTrace { get; set; }
            [CsvColumn(Name = "Payload", FieldIndex = 6)]
            public string Payload { get; set; }
        }

        /// <summary>
        /// To run this:
        /// 1. First run the RabbitErrorQueueParser.
        /// 2. Take the correlation ids from the correlation_log.txt file ane query for the correlation id and order created message in the order lifecycle database:
        ///     SELECT TOP (1000) [CorrelationId],[OrderCreatedMessage]
        ///         FROM[OrderLifecycle].[dbo].[CreateSalesOrderSaga]
        ///         where CorrelationId in (...)
        /// 3. Copy the output of that query and replace the contents of the CorrelationsFromDatabase.txt file.
        /// 4. Run this program.
        ///
        /// This program will output fresh "NewSkusWithAccounts.csv" which are the SKUs & accounts for skus we haven't reported before.
        /// In addition, it will append the newly discovered skus to the SKUsAlreadyReported file.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            const string shipmentErrorsFilePath = "T:\\BuildASign\\OrderLifecycleAnalyzers\\src\\GetShipmentIds\\ShipmentMustHaveABoxedDate.csv";
            const string commandIdsFilePath = "T:\\BuildASign\\OrderLifecycleAnalyzers\\src\\GetShipmentIds\\commandIds.txt";
            const string eventsFilePath = "T:\\BuildASign\\OrderLifecycleAnalyzers\\src\\GetShipmentIds\\events.txt";

            var csvContext = new CsvContext();

            var rawErrorRecords = csvContext.Read<ErrorCorrelationInfo>(shipmentErrorsFilePath, new CsvFileDescription {SeparatorChar = ',', FirstLineHasColumnNames = false, EnforceCsvColumnAttribute = true});

            var commandIds = new List<string>();
            foreach (var rawErrorRecord in rawErrorRecords)
            {
                var commandIdStart = rawErrorRecord.Payload.IndexOf("commandId");
                if (commandIdStart == -1)
                {
                    // Without a correlation ID, we can't use this record.
                    continue;
                }

                var commandIdString = rawErrorRecord.Payload.Substring(commandIdStart + 13, 36);
                if (!Guid.TryParse(commandIdString, out var correlationId))
                {
                    // If this isn't a valid GUID we can't use it to correlate the account number anyway so just skip.
                    continue;
                }

                commandIds.Add($"'{commandIdString}',");
            }

            File.WriteAllLines(commandIdsFilePath, commandIds);

            var eventLines = File.ReadAllLines(eventsFilePath);

            foreach (var eventLine in eventLines)
            {
                if (eventLine.Length < 200)
                {
                    // This line couldn't possibly have the info we are looking for.
                    continue;
                }

                var dateString = eventLine.Substring(0, 31).Trim();
                if (!DateTime.TryParse(dateString, out var date))
                {
                    // If this isn't a valid date the rest of the line is probably not formatted correctly and it'll just lead to errors.
                    continue;
                }
            }
        }
    }
}
