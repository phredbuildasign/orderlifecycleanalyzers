﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LINQtoCSV;

namespace RabbitErrorQueueParser
{
    class Program
    {
        private class DbCorrelationInfo
        {
            public Guid CorrelationId { get; set; }
            public string OrderId { get; set; }
            public DateTime OrderCreatedDate { get; set; }
        }

        private class ErrorCorrelationInfo
        {
            [CsvColumn(Name = "Date", FieldIndex = 1)]
            public string Date { get; set; }
            [CsvColumn(Name = "Message", FieldIndex = 3)]
            public string Message { get; set; }
            [CsvColumn(Name = "ExceptionType", FieldIndex = 4)]
            public string ExceptionType { get; set; }
            [CsvColumn(Name = "ErrorMessage", FieldIndex = 5)]
            public string ErrorMessage { get; set; }
            [CsvColumn(Name = "StackTrace", FieldIndex = 6)]
            public string StackTrace { get; set; }
            [CsvColumn(Name = "Payload", FieldIndex = 7)]
            public string Payload { get; set; }
        }

        private class ErrorWithOrderId : ErrorCorrelationInfo
        {
            [CsvColumn(Name = "Legacy Order Id", FieldIndex = 2)]
            public string OrderId { get; set; }
        }

        /// <summary>
        /// To run this:
        /// 1. First run the RabbitErrorQueueParser.
        /// 2. Take the correlation ids from the correlation_log.txt file ane query for the correlation id and order created message in the order lifecycle database:
        ///     SELECT TOP (1000) [CorrelationId],[OrderCreatedMessage]
        ///         FROM[OrderLifecycle].[dbo].[CreateSalesOrderSaga]
        ///         where CorrelationId in (...)
        /// 3. Copy the output of that query and replace the contents of the CorrelationsFromDatabase.txt file.
        /// 4. Run this program.
        ///
        /// This program will output fresh "NewSkusWithAccounts.csv" which are the SKUs & accounts for skus we haven't reported before.
        /// In addition, it will append the newly discovered skus to the SKUsAlreadyReported file.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var queueNames = new List<string>
            {
                "create_sales_order",
                "deferred_revenue"
            };

            const string errorsWithOrderIdsFilePathTemplate = "T:\\BuildASign\\OrderLifecycleAnalyzers\\src\\CorrelationParserForAllRecords\\ErrorsWithOrderIds_{0}.csv";
            const string errorLinesFilePathTemplate = "T:\\BuildASign\\OrderLifecycleAnalyzers\\src\\RabbitErrorQueueParser\\error_log_{0}.csv";
            const string correlationLogFilePathTemplate = "T:\\BuildASign\\OrderLifecycleAnalyzers\\src\\RabbitErrorQueueParser\\correlation_log_{0}.txt";
            const string correlationsFromDatabaseFilePathTemplate = "T:\\BuildASign\\OrderLifecycleAnalyzers\\src\\CorrelationParserForAllRecords\\CorrelationsFromDatabase_{0}.txt";

            var now = DateTime.Now;

            foreach (var currentQueueName in queueNames)
            {
                var correlationLogFileName = string.Format(correlationLogFilePathTemplate, currentQueueName);
                if (!File.Exists(correlationLogFileName) || (now - new FileInfo(correlationLogFileName).LastWriteTime).TotalMinutes > 30) continue;
                var correlationsFromDatabaseFileName = string.Format(correlationsFromDatabaseFilePathTemplate, currentQueueName);
                if (!File.Exists(correlationsFromDatabaseFileName) || (now - new FileInfo(correlationsFromDatabaseFileName).LastWriteTime).TotalMinutes > 30) continue;
                var errorFileName = string.Format(errorLinesFilePathTemplate, currentQueueName);
                if (!File.Exists(errorFileName) || (now - new FileInfo(errorFileName).LastWriteTime).TotalMinutes > 30) continue;

                var correlationLogLines = File.ReadAllLines(correlationLogFileName);
                var correlationDbLines = File.ReadAllLines(correlationsFromDatabaseFileName);

                var csvContext = new CsvContext();

                var rawErrorRecords = csvContext.Read<ErrorCorrelationInfo>(errorFileName, new CsvFileDescription {SeparatorChar = ',', FirstLineHasColumnNames = true});

                var correlatedErrorRecords = new Dictionary<Guid, List<ErrorCorrelationInfo>>();

                foreach (var rawErrorRecord in rawErrorRecords)
                {
                    var correlationIdStart = rawErrorRecord.Payload.IndexOf("correlationId");
                    if (correlationIdStart == -1)
                    {
                        // Without a correlation ID, we can't use this record.
                        continue;
                    }

                    var correlationIdString = rawErrorRecord.Payload.Substring(correlationIdStart + 17, 36);
                    if (!Guid.TryParse(correlationIdString, out var correlationId))
                    {
                        // If this isn't a valid GUID we can't use it to correlate the account number anyway so just skip.
                        continue;
                    }

                    if (!correlatedErrorRecords.ContainsKey(correlationId))
                    {
                        correlatedErrorRecords.Add(correlationId, new List<ErrorCorrelationInfo>());
                    }

                    correlatedErrorRecords[correlationId].Add(rawErrorRecord);
                }

                var correlatedLogRecords = new Dictionary<Guid, string>();

                foreach (var correlationLogLine in correlationLogLines)
                {
                    if (correlationLogLine.Length < 300)
                    {
                        // This line couldn't possibly have the info we are looking for.
                        continue;
                    }

                    var dateString = correlationLogLine.Substring(0, 31).Trim();
                    if (!DateTime.TryParse(dateString, out var date))
                    {
                        // If this isn't a valid date the rest of the line is probably not formatted correctly and it'll just lead to errors.
                        continue;
                    }

                    var errorMessage = correlationLogLine.Substring(31, 230).Trim();
                    if (!errorMessage.StartsWith("An error occurred while attempting to translate SKUs. A SKU mapping may be missing. Original error message: Could not determine the product line matching the SKU"))
                    {
                        // This isn't the SKU message you're looking for.
                        continue;
                    }

                    var correlationIdString = correlationLogLine.Substring(263, 36);
                    if (!Guid.TryParse(correlationIdString, out var correlationId))
                    {
                        // If this isn't a valid GUID we can't use it to correlate the account number anyway so just skip.
                        continue;
                    }

                    correlatedLogRecords.Add(correlationId, errorMessage.Substring(163).Replace("'", string.Empty));
                }

                var correlatedDbRecords = new Dictionary<Guid, DbCorrelationInfo>();

                foreach (var orderCreatedLine in correlationDbLines)
                {
                    if (orderCreatedLine.Length < 72)
                    {
                        // This line couldn't possibly have the info we are looking for.
                        continue;
                    }

                    var correlationIdString = orderCreatedLine.Substring(0, 36);
                    if (!Guid.TryParse(correlationIdString, out var correlationId))
                    {
                        // If this isn't a valid date the rest of the line is probably not formatted correctly and it'll just lead to errors.
                        continue;
                    }

                    var orderId = orderCreatedLine.Substring(58, 11).Trim().Replace("\"", string.Empty).Replace(",", string.Empty);

                    var t = orderCreatedLine.Substring(orderCreatedLine.IndexOf("CreatedDate") + 14, 27);
                    var orderCreatedDateString = t.IndexOf("\"") == -1 ? t : t.Substring(0, t.IndexOf("\""));
                    var orderCreatedDate = DateTime.Parse(orderCreatedDateString);

                    correlatedDbRecords.Add(correlationId, new DbCorrelationInfo {CorrelationId = correlationId, OrderId = orderId, OrderCreatedDate = orderCreatedDate});
                }

                var errorsWithOrderIds = new List<ErrorWithOrderId>();

                foreach (var errorRecords in correlatedErrorRecords)
                {
                    string orderId;
                    if (!correlatedDbRecords.ContainsKey(errorRecords.Key))
                    {
                        // Hmmmm, must not exist in the database, no order ID to be had.
                        orderId = string.Empty;
                    }
                    else
                    {
                        orderId = correlatedDbRecords[errorRecords.Key].OrderId;
                    }

                    foreach (var errorRecord in errorRecords.Value)
                    {
                        var fullErrorRecord = new ErrorWithOrderId
                        {
                            Date = errorRecord.Date,
                            OrderId = orderId,
                            Message = errorRecord.Message,
                            ErrorMessage = errorRecord.ErrorMessage,
                            StackTrace = errorRecord.StackTrace,
                            Payload = errorRecord.Payload
                        };
                        errorsWithOrderIds.Add(fullErrorRecord);
                    }
                }

                var sortedErrors = errorsWithOrderIds.OrderBy(rec => rec.ErrorMessage).ThenBy(rec => rec.OrderId).ThenBy(rec => rec.Date).ToList();
                csvContext.Write(sortedErrors, string.Format(errorsWithOrderIdsFilePathTemplate, currentQueueName), new CsvFileDescription {SeparatorChar = ',', FirstLineHasColumnNames = true});
            }
        }
    }
}
