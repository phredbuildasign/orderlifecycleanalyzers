﻿using System.Collections.Generic;

namespace RabbitErrorQueueParser
{
    public static class CsvExtension
    {
        public static string FormatForCsv(this string value)
        {
            return value.Replace("\"", "'");
        }

        public static string[] ParseFromCsv(this string csvString)
        {
            var parts = csvString.Split(',');
            var parsedParts = new List<string>();
            foreach (var part in parts)
            {
                if (part.StartsWith("\""))
                {
                    parsedParts.Add(part.Substring(1, part.Length - 2));
                }
                else
                {
                    parsedParts.Add(part);
                }
            }
            return parsedParts.ToArray();
        }
    }
}
