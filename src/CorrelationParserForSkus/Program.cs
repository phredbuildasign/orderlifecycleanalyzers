﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace RabbitErrorQueueParser
{
    class Program
    {
        private class DbCorrelationInfo
        {
            public Guid CorrelationId { get; set; }
            public string OrderId { get; set; }
            public DateTime OrderCreatedDate { get; set; }
        }

        /// <summary>
        /// To run this:
        /// 1. First run the RabbitErrorQueueParser.
        /// 2. Take the correlation ids from the correlation_log.txt file ane query for the correlation id and order created message in the order lifecycle database:
        ///     SELECT TOP (1000) [CorrelationId],[OrderCreatedMessage]
        ///         FROM[OrderLifecycle].[dbo].[CreateSalesOrderSaga]
        ///         where CorrelationId in (...)
        /// 3. Copy the output of that query and replace the contents of the CorrelationsFromDatabase.txt file.
        /// 4. Run this program.
        ///
        /// This program will output fresh "NewSkusWithOrders.csv" which are the SKUs & orders for skus we haven't reported before.
        /// In addition, it will append the newly discovered skus to the SKUsAlreadyReported file.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            const string newSkusWithAccountsFilePath = "T:\\BuildASign\\OrderLifecycleAnalyzers\\src\\CorrelationParserForSkus\\NewSkusWithOrderIds.csv";
            const string skusAlreadyreportedFilePath = "T:\\BuildASign\\OrderLifecycleAnalyzers\\src\\CorrelationParserForSkus\\SKUsAlreadyReported.csv";

            var correlationLines = File.ReadAllLines("T:\\BuildASign\\OrderLifecycleAnalyzers\\src\\RabbitErrorQueueParser\\correlation_log.txt");
            var orderCreatedLines = File.ReadAllLines("T:\\BuildASign\\OrderLifecycleAnalyzers\\src\\CorrelationParserForSkus\\CorrelationsFromDatabase.txt");
            var alreadyReportedSkuLines = File.Exists(skusAlreadyreportedFilePath) ? File.ReadAllLines(skusAlreadyreportedFilePath) : new string[0];

            var skuRecords = new Dictionary<Guid, string>();

            foreach (var correlationLine in correlationLines)
            {
                if (correlationLine.Length < 300)
                {
                    // This line couldn't possibly have the info we are looking for.
                    continue;
                }

                var dateString = correlationLine.Substring(0, 31).Trim();
                if (!DateTime.TryParse(dateString, out var date))
                {
                    // If this isn't a valid date the rest of the line is probably not formatted correctly and it'll just lead to errors.
                    continue;
                }

                var errorMessage = correlationLine.Substring(31, 230).Trim();
                if (!errorMessage.StartsWith("An error occurred while attempting to translate SKUs. A SKU mapping may be missing. Original error message: Could not determine the product line matching the SKU"))
                {
                    // This isn't the SKU message you're looking for.
                    continue;
                }

                var correlationIdString = correlationLine.Substring(263, 36);
                if (!Guid.TryParse(correlationIdString, out var correlationId))
                {
                    // If this isn't a valid GUID we can't use it to correlate the account number anyway so just skip.
                    continue;
                }

                skuRecords.Add(correlationId, errorMessage.Substring(163).Replace("'", string.Empty));
            }

            var orderCreatedRecords = new Dictionary<Guid, DbCorrelationInfo>();

            foreach (var orderCreatedLine in orderCreatedLines)
            {
                if (orderCreatedLine.Length < 72)
                {
                    // This line couldn't possibly have the info we are looking for.
                    continue;
                }

                var correlationIdString = orderCreatedLine.Substring(0, 36);
                if (!Guid.TryParse(correlationIdString, out var correlationId))
                {
                    // If this isn't a valid date the rest of the line is probably not formatted correctly and it'll just lead to errors.
                    continue;
                }

                var orderId = orderCreatedLine.Substring(58, 11).Trim().Replace("\"", string.Empty).Replace(",", string.Empty);

                var t = orderCreatedLine.Substring(orderCreatedLine.IndexOf("CreatedDate") + 14, 27);
                var orderCreatedDateString = t.IndexOf("\"") == -1 ? t : t.Substring(0, t.IndexOf("\""));
                var orderCreatedDate = DateTime.Parse(orderCreatedDateString);

                orderCreatedRecords.Add(correlationId, new DbCorrelationInfo{CorrelationId = correlationId, OrderId = orderId, OrderCreatedDate = orderCreatedDate });
            }

            var alreadyReportedSkuRecords = new HashSet<string>();

            foreach (var alreadyReportedSkuLine in alreadyReportedSkuLines)
            {
                alreadyReportedSkuRecords.Add(alreadyReportedSkuLine);
            }

            var uniqueCsvAccountsAndSkus = new List<string>();
            var uniqueCsvAccountsAndSkusAndCreationDates = new List<string>();

            foreach (var skuRecord in skuRecords)
            {
                if (!orderCreatedRecords.ContainsKey(skuRecord.Key))
                {
                    // Hmmmm, must not exist in the database, ignore.
                    continue;
                }

                var skuAccountRecord = $"\"{skuRecord.Value.FormatForCsv()}\",{orderCreatedRecords[skuRecord.Key].OrderId}";
                if (alreadyReportedSkuLines.Contains(skuAccountRecord))
                {
                    // We've already reported this one. Skip it.
                    continue;
                }

                uniqueCsvAccountsAndSkus.Add(skuAccountRecord);
                uniqueCsvAccountsAndSkusAndCreationDates.Add($"{skuAccountRecord},\"{orderCreatedRecords[skuRecord.Key].OrderCreatedDate}\"");
            }

            uniqueCsvAccountsAndSkus.Sort();
            File.WriteAllLines(newSkusWithAccountsFilePath, new [] { "\"SKU Message\",\"Legacy OrderId\",\"Order Created Date/Time\"" });

            File.AppendAllLines(newSkusWithAccountsFilePath, uniqueCsvAccountsAndSkusAndCreationDates);
            File.AppendAllLines(skusAlreadyreportedFilePath, uniqueCsvAccountsAndSkus);
        }
    }
}
