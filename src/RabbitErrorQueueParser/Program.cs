﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using LINQtoCSV;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace RabbitErrorQueueParser
{
    class Program
    {
        private class ErrorInfo
        {
            [DisplayName("Date")]
            public string Date { get; set; }
            [DisplayName("Message")]
            public string Message { get; set; }
            [DisplayName("Exception Type")]
            public string ExceptionType { get; set; }
            [DisplayName("Error Message")]
            public string ErrorMessage { get; set; }
            [DisplayName("Stack Track")]
            public string StackTrace { get; set; }
            [DisplayName("Payload")]
            public string Payload { get; set; }
        }

        // NETSUITE
        //private static string[] errorMessagesToExcludeFirst35 = {
        //    "DUP_ENTITY: This entity already exi",
        //    "USER_ERROR: You must enter at least one line item for this transaction.".Substring(0, 35),
        //    "INVALID_KEY_OR_REF: The specified key is invalid.".Substring(0, 35),
        //    "RCRD_HAS_BEEN_CHANGED: Record has been changed".Substring(0, 35),
        //    "Sales order could not be found with External ID ".Substring(0, 35),
        //    "SalesOrder could not be found with external ID ".Substring(0, 35),
        //    "The operation has timed out"
        //};

        // ORDER LIFECYCLE
        private static readonly string[] _errorMessagesToExclude = {
            "USER_ERROR: You cannot complete this action now because another user is currently creating an Assembly Build for this Work Order. Please try again later\\.",
            "The line item with Line Item ID L[0-9]+ cannot be canceled because it has already been built\\.",
            "USER_ERROR: Unable to find a matching line for sublist deposit with key: \\[doc,line\\] and value: \\[[0-9]+,null\\]\\.",
            "USER_ERROR: You cannot create an inventory detail for this item.",
            "INVALID_WO: You have an invalid work order [0-9]+ or the order is already closed.",
            "UNEXPECTED_ERROR: An unexpected error occurred. Error ID: [a-z0-9]+",
            "USER_ERROR: The total inventory detail quantity must be [0-9\\.]+\\."
        };

        /// <summary>
        /// -1. The HtmlAgilityPack package doesn't exist in the BAS MyGet account, disable it and enable nuget.org in your Tools->NuGet Packages settings to do the restore.
        /// 0. Clear out the rows in error_log.csv if you don't want to append there.
        /// 1. Get messages from error queue without requeueing.
        /// 2. Hit F12 on the browser and copy the HTML into the input.html file.
        /// 3. Run this program.
        /// 4. Import the csv results into the google sheet: https://docs.google.com/spreadsheets/d/1Yi9VxNEpmlNBkcWKAYYUh2KWnLUD5knumCGWhL4ejQk/edit#gid=1043096621
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var queueNames = new List<string>
            {
                "order_lifecycle_add_reprints",
                "order_lifecycle_create_sales_order",
                "order_lifecycle_ecommerce_notifications",
                "order_lifecycle_events",
                "order_lifecycle_refunds",
                "order_lifecycle_deferred_revenue",
                "order_lifecycle_production_information",
                "netsuite_shipping_notifications",
                "netsuite_reprints",
                "netsuite_updates",
                "netsuite_refunds",
                "netsuite_salesorder"
            };

            var myDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var inputsBase = myDocuments + "\\Daily Order Lifecycle Inputs";
            var outputsBase = myDocuments + "\\Daily Order Lifecycle Errors";
            var inputHtmlFilePathTemplate = inputsBase + "\\{0}_error_input.html";
            var errorLogFilePathTemplate = outputsBase + "\\{0}_error_log_{1}.xlsx";
            var csvErrorLogFilePathTemplate = outputsBase + "\\error_log_{0}.csv";

            var files = new DirectoryInfo(inputsBase).GetFileSystemInfos();
            var youngestFileTime = files.Max(fsi => fsi.LastWriteTime);

            // If no input files have been updated lately exit early.
            if ((DateTime.Now - youngestFileTime).TotalHours > 15) return;

            var now = youngestFileTime;
            var today = now.ToString("MM_dd_hh_mm");
            var yesterday = now.DayOfWeek == DayOfWeek.Monday ? now.AddDays(-3.5) : now.AddDays(-1.5);

            foreach (var currentQueueName in queueNames)
            {
                var inputFileName = string.Format(inputHtmlFilePathTemplate, currentQueueName);

                // Skip empty and old files.
                var inputFileInfo = new FileInfo(inputFileName);
                if (inputFileInfo.Length < 10 || (now - inputFileInfo.LastWriteTime).TotalHours > 15) continue;

                var doc = new HtmlDocument();
                doc.Load(inputFileName);

                var messages = doc.DocumentNode.SelectNodes("//div[@id='msg-wrapper']/div/table[@class='facts']");

                var csvRowErrorMessage = new List<ErrorInfo>();
                var correlationMessages = new List<string>();

                foreach (var message in messages)
                {
                    var dateNode = message.SelectNodes("./tbody/tr/td/table[@class='mini']/tbody/tr/td/table/tbody/tr[position()=5]/td");
                    var exceptionTypeNode = message.SelectNodes("./tbody/tr/td/table[@class='mini']/tbody/tr/td/table/tbody/tr[position()=2]/td");
                    var errorMessageNode = message.SelectNodes("./tbody/tr/td/table[@class='mini']/tbody/tr/td/table/tbody/tr[position()=3]/td");
                    ErrorInfo errorInfo = null;

                    if (dateNode != null)
                    {
                        errorInfo = new ErrorInfo
                        {
                            Date = dateNode.First().InnerText,
                            ExceptionType = exceptionTypeNode == null ? string.Empty : exceptionTypeNode.First().InnerText,
                            ErrorMessage = errorMessageNode == null ? string.Empty : errorMessageNode.First().InnerText
                        };
                    }

                    if (errorInfo == null)
                    {
                        continue;
                    }

                    if (IsExcludedError(errorInfo))
                    {
                        continue;
                    }

                    if (DateTime.TryParse(errorInfo.Date, out var recordDate))
                    {
                        if (recordDate < yesterday)
                        {
                            continue;
                        }
                    }

                    var stackTraceNode = message.SelectNodes("./tbody/tr/td/table[@class='mini']/tbody/tr/td/table/tbody/tr[position()=4]/td/abbr");
                    errorInfo.StackTrace = RemoveLeadingSpaces(stackTraceNode == null ? string.Empty : stackTraceNode.First().InnerText);

                    errorInfo.Payload = message.SelectNodes("./tbody//pre[@class='msg-payload']").First().InnerText;
                    errorInfo.Message = Regex.Match(errorInfo.Payload, "urn:message:(.*)\"").Groups[1].Value;

                    if (errorInfo.ErrorMessage.Equals("Source sequence doesn't contain any elements.") &&
                        errorInfo.ExceptionType.Equals("System.InvalidOperationException") &&
                        errorInfo.StackTrace.Contains("GetSalesOrderIdByLegacyIdQueryHandler"))
                    {
                        var orderIdIndex = errorInfo.Payload.IndexOf("orderId", StringComparison.InvariantCultureIgnoreCase);
                        var orderIdToEndString = orderIdIndex == -1 ? "Unknown" : errorInfo.Payload.Substring(orderIdIndex + 11);
                        var orderIdToEndStringFirstQuote = orderIdToEndString.IndexOf('"');
                        var orderId = orderIdToEndStringFirstQuote == -1 ? orderIdToEndString.Substring(0, Math.Min(8, orderIdToEndString.Length)) : orderIdToEndString.Substring(0, orderIdToEndStringFirstQuote);
                        errorInfo.ErrorMessage = $"The SalesOrder was not found for LegacyId = {orderId}";
                    }

                    csvRowErrorMessage.Add(errorInfo);
                    string correlationId = errorInfo.Payload.Contains("correlationId") ? errorInfo.Payload.Substring(errorInfo.Payload.LastIndexOf("correlationId") + 17, 36) : string.Empty;
                    var correlationString = $"{errorInfo.Date,30} {errorInfo.ErrorMessage.FormatForCsv(),-230} '{correlationId}',";
                    correlationMessages.Add(correlationString);
                }

                var sortedErrors = csvRowErrorMessage.OrderBy(rec => rec.StackTrace).ThenBy(rec => rec.Message).ThenBy(rec => rec.ErrorMessage).ThenBy(rec => rec.Date).ToList();

                ExcelPackage pck = new ExcelPackage();
                ExcelWorksheet ew = pck.Workbook.Worksheets.Add("Errors");

                ew.CustomHeight = false;
                ew.InsertColumn(1, 6);
                ew.Column(1).Width = 29;
                ew.Column(2).Style.WrapText = true;
                ew.Column(2).Width = 30;
                ew.Column(3).Width = 30;
                ew.Column(3).Style.WrapText = true;
                ew.Column(4).Width = 30;
                ew.Column(4).Style.WrapText = true;
                ew.Column(5).Width = 90.0;
                ew.Column(6).Width = 120.0;
                ew.InsertRow(1, sortedErrors.Count + 1);
                for (int n = 2; n <= sortedErrors.Count; n++)
                {
                    ew.Row(n).CustomHeight = false;
                    ew.Row(n).Height = 300;
                }

                ew.Cells.LoadFromCollection(sortedErrors, true);

                foreach (var currentCell in ew.Cells)
                {
                    currentCell.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    currentCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                }

                ew.Column(2).Style.WrapText = true;
                ew.Column(2).Width = 30;

                var outputFile = new FileInfo(string.Format(errorLogFilePathTemplate, currentQueueName, today));
                ew.View.ZoomScale = 75;
                pck.SaveAs(outputFile);

                if (currentQueueName == "create_sales_order" || currentQueueName == "deferred_revenue")
                {
                    var csvContext = new CsvContext();
                    csvContext.Write(sortedErrors, string.Format(csvErrorLogFilePathTemplate, currentQueueName), new CsvFileDescription { SeparatorChar = ',', FirstLineHasColumnNames = true });

                    File.WriteAllLines(outputsBase + $"\\correlation_log_{currentQueueName}.txt", correlationMessages);
                }
            }
        }

        private static string RemoveLeadingSpaces(string stringToClean)
        {
            var lines = stringToClean.Split(
                new[] { Environment.NewLine },
                StringSplitOptions.None);
            var cleanLines = new List<string>();
            foreach (var currentLine in lines)
            {
                cleanLines.Add(currentLine.Trim());
            }

            return string.Join(Environment.NewLine, cleanLines);
        }

        private static bool IsExcludedError(ErrorInfo errorInfo)
        {
            var messageToCheck = errorInfo.ErrorMessage;

            return _errorMessagesToExclude.Any(exem =>
            {
                var regEx = new Regex(exem, RegexOptions.IgnoreCase);
                return regEx.IsMatch(messageToCheck);
            });
        }
    }
}
