# Hacky Rabbit Order Lifecycle Error Queue Parser

**Note the input and output file paths are hard-coded to T:\BuildASign\OrderLifecycleAnalyzers\RabbitErrorQueueParser and T:\BuildASign\OrderLifecycleAnalyzers\CorrelationParser so you'll need to change those if you checked out this repo in a different folder**

## Instructions For RabbitErrorQueueParser

1. Use the RabbitMQ UI to get a few thousand messages (you can select the "Ack message requeue false" mode so that the messages you grab will be removed from the queue)
2. Use the browser's developer tools (F12) to view the HTML and copy the entire page's HTML and save it off into an input.html file on the rabbit server.
3. Copy the input.html file from the rabbit server to your machine in T:\BuildASign\OrderLifecycleAnalyzers\src\RabbitErrorQueueParser
4. Run the console app
5. The output files should be populated: T:\BuildASign\OrderLifecycleAnalyzers\src\RabbitErrorQueueParser\error_log.csv
T:\BuildASign\OrderLifecycleAnalyzers\src\RabbitErrorQueueParser\correlation_log.txt

## Excluding Certain Error Messages

The static variable, errorMessageToExcludeFirst35, can be populated with a list of error messages to ignore (it's only the first 35 characters of the message).

## Instructions For CorrelationParser

1. First run the RabbitErrorQueueParser.
2. Take the correlation ids from the correlation_log.txt file ane query for the correlation id and order created message in the order lifecycle database:
    SELECT TOP (1000) [CorrelationId],[OrderCreatedMessage]
        FROM[OrderLifecycle].[dbo].[CreateSalesOrderSaga]
        where CorrelationId in (...)
3. Copy the output of that query and replace the contents of the CorrelationsFromDatabase.txt file.
4. Run this program.

This program will output fresh "NewSkusWithAccounts.csv" which are the SKUs & accounts for skus we haven't reported to Accounting before.
In addition, it will append the newly discovered skus to the SKUsAlreadyReported file.


